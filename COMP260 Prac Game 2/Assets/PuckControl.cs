﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour {

	public AudioClip wallCollideClip;
	private AudioSource audio;
	public AudioClip paddleCollideClip;
	public LayerMask paddleLayer;
	public Transform startingPos;
	private Rigidbody rigidbody;

	void Start () {
		audio = GetComponent<AudioSource>();
		rigidbody = GetComponent<Rigidbody>();
		ResetPosition();
	}

	public void ResetPosition() {
		// teleport to the starting position
		rigidbody.MovePosition(startingPos.position);
		// stop it from moving
		rigidbody.velocity = Vector3.zero;
	}

	void OnCollisionEnter(Collision collision) {
		audio.PlayOneShot(wallCollideClip);
		// check what layer we have hit
		if (paddleLayer.Contains(collision.gameObject)) {
			// hit the paddle
			audio.PlayOneShot(paddleCollideClip);
		}
		else {
			// hit something else
			audio.PlayOneShot(wallCollideClip);
		}
	}    
			


}

	
